# Repohoster
A simple set of scripts to host a git repository or local directory via httpd.

## Running
To start the application, the `run.sh` script is to be used, however it is
strongly recommended to run it in a docker container instead. For this purpose
the `docker_run.sh` script is available. Arguments to the two scripts should
be the same. However, volume mounting can be tricky with docker and scripts. To
this end, another wrapper exists to wrap around `docker_run.sh` called
`docker_repohoster.sh`. It consumes the `-r` parameter to make sure that the
supplied path is volume mounted.

To host a local directory, that is not a git repository, point the script to
the directory.
```console
./docker_repohoster.sh -r <path/to/directory>
```

If the directory is a git repository, but it should be treated as a local
directory, the *-f* flag can be added to force it to be treated as a directory.
```console
./docker_repohoster.sh -f -r <path/to/directory>
```

Common behavior however is to supply a git repository, as the script will
attempt to host each branch as an individual directory. This works with either
local repositories or with URL's, as the script will clone the repository
internally.
```console
./docker_repohoster.sh -r <URL>|<path/to/repository>
```

> _NOTE_: To facilitate ease of use from outside the repository, a wrapper
> to ensure proper passing of volume parameters is available.
> `docker_repohoster.sh` It is recommended to create a symlink to this script
> from `~/bin/`.
> `ln -s ~/src/esbs/repohoster/docker_repohoster.sh ~/bin/repohoster.sh`

If no repository is supplied, the current working directory (pwd) is used
instead.

Important to note that the script avoids having to deal with authorizations.
Some work-a-rounds are:
* Use a https URL that includes the credentials as part of the URL
* Use the environment variable OPT_DOCKER_ARGS to volume-mount ssh keys
* Clone the repo first and treat it as a local directory

The container will by default publish port 80, and thus *http://localhost/...*
can be used. To override the port the environment variable REPO_PORT can be
supplied.
```console
REPO_PORT="8080" ./docker_repohoster.sh -r <URL>|<path/to/repository>
```

The docker wrapper just tries to clone the repo and start it with the proper
arguments. To avoid building the container and use the upstream container
instead, set the environment variable CI_REGISTRY_IMAGE.
```console
CI_REGISTRY_IMAGE="<URL>:tag" ./docker_repohoster.sh -r <URL>
```
Note, that if pulling of the image fails, the script will still try to build
the container.

## Manually running
To manually invoke the container using the name 'repohoster' the following
could be used.

```console
docker run \
       --hostname "$(hostname)" \
       --interactive \
       --name "repohoster" \
       --publish "${REPO_PORT:-80}:80/tcp" \
       --rm \
       --tty \
       --workdir "/workdir" \
       "${CI_REGISTRY_IMAGE}" \
       "repohoster.sh"

docker exec \
       --interactive \
       "repohoster" \
       "reposync.sh" -r "<URL>|<path/to/repository>"
```

The above example does not deal with volume mounts for local directories, etc
and are left as an exercise to the reader.

The example can be used however as part of a longer living CI job, where
the repohoster container is run initially, and the reposync tool is used to
either initially create/sync the repository and/or keep it 'up-to-date'.

# Available programs in the container
The container has three workhorse scripts `repohoster.sh`, to host a repository
and `reposync.sh` to synchronize a git repository and `index.cgi` to render
a html representation of a directory structure.

## Repohoster
Repohoster is a simple script, that prepares the web root and launches
busybox's httpd to expose the web root directory. To prepare the web root it
creates symlink to `index.cgi` in the web root's `cgi-bin` directory. If this
fails already is available, html generation may not work as expected.

## Reposync
Reposync tries to either clone a repository from the supplied argument into
the web root, or updates an existing repository inside the web root. Further
more it creates worktree's for each branch and exposes these as subdirectories
of the to be hosted repository.
```
.
├── repo1
│   ├── master
│   ├── branch2
│   └── branch3
└── repo2
```

## Directory rendering
A very simple shell script is included to perform rendering of a directory
structure. For performance reasons, this could be replaced by a compiled
program, for now however, this can only easily be done by supplying a directory
rather then an URL or repository to host and placing the `cgi-bin/index.cgi`
binary in the directory manually.
