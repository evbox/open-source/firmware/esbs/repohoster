#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu


src_file="$(readlink -f "${0}")"
src_dir="${src_file%%"${src_file##*'/'}"}"

while getopts ':r:' _options; do
	case "${_options}" in
	r)
		_repo="${OPTARG}"
		if [ -e "${_repo:-}" ]; then
			_repo="$(readlink -f "${_repo}")"
			opt_docker_args="--volume '${_repo}:${_repo}' ${opt_docker_args:-}"
		fi
		;;
	*) # Pass everything else along
		;;
	esac
done

OPT_DOCKER_ARGS="${OPT_DOCKER_ARGS:-}${opt_docker_args:-}" "${src_dir}/docker_run.sh" "${@}"

exit 0
